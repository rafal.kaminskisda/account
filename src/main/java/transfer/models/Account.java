package transfer.models;

public class Account {

    int balance;
    String accountNumber;
    String firstName;
    String lastName;

    public Account(int balance, String accountNumber, String firstName, String lastName) {
        this.balance = balance;
        this.accountNumber = accountNumber;
        this.firstName = firstName;
        this.lastName = lastName;
    }


    public int getBalance() {
        return this.balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getAccountNumber() {
        return this.accountNumber;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }
}
