package transfer;

import org.junit.jupiter.api.Test;
import transfer.models.Account;

import static org.junit.jupiter.api.Assertions.*;

public class AccountTest {
    
    @Test
    void shouldTransferCash(){
        //given
        Account recipient = new Account(0, "1234567890", "Ewa", "Farna");
        Account sender = new Account(10, "1234567890", "Lewis", "Hamilton");

        //when
        Transfer.doTransfer(recipient, sender, 10);

        //then
        assertEquals(0, sender.getBalance());
        assertEquals(10, recipient.getBalance());
    }

    @Test
    void shouldNumberBeInvalidToLessDigits(){
        //given
        Account recipient = new Account(0, "123456789", "Ewa", "Farna");
        Account sender = new Account(10, "1234567890", "Lewis", "Hamilton");

        //when
        Transfer.doTransfer(recipient, sender, 10);

        //then
        assertEquals(10, sender.getBalance());
        assertEquals(0, recipient.getBalance());
    }

    @Test
    void shouldNumberBeInvalidToManyDigits(){
        //given
        Account recipient = new Account(0, "12345678901", "Ewa", "Farna");
        Account sender = new Account(10, "1234567890", "Lewis", "Hamilton");

        //when
        Transfer.doTransfer(recipient, sender, 10);

        //then
        assertEquals(10, sender.getBalance());
        assertEquals(0, recipient.getBalance());
    }

    @Test
    void shouldNumberBeInvalidWithLetters() {
        //given
        Account recipient = new Account(0, "12345678A0", "Ewa", "Farna");
        Account sender = new Account(10, "1234567890", "Lewis", "Hamilton");

        //when
        Transfer.doTransfer(recipient, sender, 10);

        //then
        assertEquals(10, sender.getBalance());
        assertEquals(0, recipient.getBalance());
    }

    @Test
    void shouldNumberBeInvalidWhenNull() {
        //given
        Account recipient = new Account(0, null, "Ewa", "Farna");
        Account sender = new Account(10, "1234567890", "Lewis", "Hamilton");

        //when
        Transfer.doTransfer(recipient, sender, 10);

        //then
        assertEquals(10, sender.getBalance());
        assertEquals(0, recipient.getBalance());
    }

    @Test
    void shouldAccountDataBeInvalidWhenInvalidNumber(){
        //given
        Account recipient = new Account(0, "", "Ewa", "Farna");
        Account sender = new Account(10, "1234567890", "Lewis", "Hamilton");

        //when
        Transfer.doTransfer(recipient, sender, 10);

        //then
        assertEquals(10, sender.getBalance());
        assertEquals(0, recipient.getBalance());
    }

    @Test
    void shouldAccountDataBeInvalidWhenNullName(){
        //given
        Account recipient = new Account(0, "1234567890", null, "Farna");
        Account sender = new Account(10, "1234567890", "Lewis", "Hamilton");

        //when
        Transfer.doTransfer(recipient, sender, 10);

        //then
        assertEquals(10, sender.getBalance());
        assertEquals(0, recipient.getBalance());
    }

    @Test
    void shouldAccountDataBeInvalidWhenNullLastName(){
        //given
        Account recipient = new Account(0, "1234567890", "Ewa", null);
        Account sender = new Account(10, "1234567890", "Lewis", "Hamilton");

        //when
        Transfer.doTransfer(recipient, sender, 10);

        //then
        assertEquals(10, sender.getBalance());
        assertEquals(0, recipient.getBalance());
    }

    @Test
    void shouldAccountDataBeInvalidWhenEmptyName(){
        //given
        Account recipient = new Account(0, "1234567890", "", "Farna");
        Account sender = new Account(10, "1234567890", "Lewis", "Hamilton");

        //when
        Transfer.doTransfer(recipient, sender, 10);

        //then
        assertEquals(10, sender.getBalance());
        assertEquals(0, recipient.getBalance());
    }

    @Test
    void shouldAccountDataBeValidWhenEmptyLastName(){
        //given
        Account recipient = new Account(0, "1234567890", "Ewa", "");
        Account sender = new Account(10, "1234567890", "Lewis", "Hamilton");

        //when
        Transfer.doTransfer(recipient, sender, 10);

        //then
        assertEquals(10, sender.getBalance());
        assertEquals(0, recipient.getBalance());
    }

    @Test
    void shouldNotHaveEnoughCash(){
        //given
        Account recipient = new Account(0, "", "", "");
        Account sender = new Account(9, "1234567890", "Lewis", "Hamilton");
        int cashToTransfer = 10;

        //when
        Transfer.doTransfer(recipient, sender, cashToTransfer);

        //then
        assertEquals(9, sender.getBalance());
        assertEquals(0, recipient.getBalance());
    }
}
